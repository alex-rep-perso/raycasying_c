CC = gcc
CFLAGS = -O2 -DNDEBUG -std=c99 -pedantic -Wall -Wextra -Werror
OBJS = src/main.o                                                      \
src/elements.o src/environment.o src/list_sphere.o                     \
src/vector.o src/vector2.o src/camera.o src/functions.o src/light.o    \
src/render.o src/raycast.o src/shading.o src/plane.o src/triangle.o    \
src/struct.o src/processing.o

EXEC = rt


all: rt

rt: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(EXEC) -lm


clean:
	rm -rf $(OBJS) $(EXEC)



.PHONY: clean all
