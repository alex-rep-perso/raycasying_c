#include "elements.h"


#ifndef LIST_SPHERE_H
# define LIST_SPHERE_H


struct list_sphere
{
    double dist;
    struct list_sphere *next;
    struct sphere *elt;
};


struct list_sphere *add_at(struct list_sphere *l, struct sphere *s);
void print_list_sphere(struct list_sphere *l);
struct list_sphere *pop_sphere(struct list_sphere * l);
void free_list_sphere(struct list_sphere *l);

#endif /* !LIST_SPHERE_H */
