#ifndef PLANE_H
# define PLANE_H

#include "elements.h"

struct list_plan
{
    struct list_plan *next;
    struct plane *elt;
};


struct plane *create_plane(char *parameter);
struct list_plan *add_plane(struct list_plan *l, struct plane *p);
void free_list_plan(struct list_plan *l);

#endif /* !PLANE_H */
