# define _GNU_SOURCE

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "vector.h"
#include "vector2.h"
#include "elements.h"
#include "functions.h"
#include "environment.h"

struct info *get_info(char *parameters)
{
    struct info *info = malloc(sizeof (struct info));

    char *saveptr = NULL;

    info->diff = my_atof(strtok_r(parameters, " ", &saveptr));
    info->refl = my_atof(strtok_r(NULL, " ", &saveptr));
    info->spec = my_atof(strtok_r(NULL, " ", &saveptr));
    info->shin = my_atof(strtok_r(NULL, " ", &saveptr));

    struct color c;
    c.r = my_atoi(strtok_r(NULL, " ", &saveptr));
    c.g = my_atoi(strtok_r(NULL, " ", &saveptr));
    c.b = my_atoi(strtok_r(NULL, " ", &saveptr));

    info->c = c;

    info->refr = my_atof(strtok_r(NULL, " ", &saveptr));
    info->opac = my_atof(strtok_r(NULL, " ", &saveptr));

    return info;
}



struct sphere *create_sphere(s_vector3f pos, char *parameters)
{
    struct sphere *s = malloc(sizeof (struct sphere));

    char *saveptr = NULL;

    strtok_r(parameters, " ", &saveptr);
    s->radius = my_atof(strtok_r(NULL, " ", &saveptr));

    s->center.x = my_atof(strtok_r(NULL, " ", &saveptr));
    s->center.y = my_atof(strtok_r(NULL, " ", &saveptr));
    s->center.z = my_atof(strtok_r(NULL, " ", &saveptr));

    s->info = get_info(strtok_r(NULL, "\n", &saveptr));

    s->dist = vec_dist(pos, s->center) - s->radius;

    return s;
}


void free_sphere(struct sphere *s)
{
    free(s->info);
    free(s);
}
