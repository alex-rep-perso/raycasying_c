#ifndef RAYCAST_H
#define RAYCAST_H

#include "list_sphere.h"
#include "plane.h"
#include "triangle.h"
#include "elements.h"
#include "camera.h"
#include "vector.h"
#include "environment.h"

double find_intersect_sphere(s_vector3f *pt_i, struct ray r, struct sphere *s);
struct sphere *find_nearest_sphere(double *dist, s_vector3f *pt, 
                                   struct ray r, struct list_sphere *l_s);

struct plane *find_nearest_plan(double *dist, s_vector3f *pt, 
                                struct ray r, struct list_plan *l_p);

struct triangle *find_nearest_tri(double *dist, s_vector3f *pt,
                                  struct ray r, struct list_triangle *l_t);
double find_intersect(struct env *e, struct ray r, s_vector3f *pt);
#endif /* !RAYCAST_H */
