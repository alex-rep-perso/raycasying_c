#include <stdlib.h>
#include <stdio.h>
#include "struct.h"

# define FOCAL 25


struct color **stretch(int *w, int *h, int fact)
{
    *w *= fact;
    *h *= fact;

    struct color **newscreen = malloc(sizeof (struct color *) * *w);

    for (int i = 0; i < *w; i++)
        newscreen[i] = malloc(sizeof (struct color) * *h);

    return newscreen;
}


static int check(int w, int h, int i, int j)
{
    return i >= 0 && i < w && j >= 0 && j < h;
}

static struct color get_color(int r, int g, int b)
{
    struct color c =
    {
        .r = r,
        .g = g,
        .b = b
    };

    return c;
}

struct color **shrink(struct color **screen, int ow, int oh, int fact)
{
    int w = ow / fact;
    int h = oh / fact;

    struct color **newscreen = malloc(sizeof (struct color *) * w);

    for (int i = 0; i < w; i++)
    {
        newscreen[i] = malloc(sizeof (struct color) * h);
        for (int j = 0; j < h; j++)
        {
            int r = 0;
            int g = 0;
            int b = 0;
            int count = 0;
            for (int k = - fact + 1; k < fact; k++)
            {
                for (int l = - fact + 1; l < fact; l++)
                {
                    if (check(w, h, i + k, j + l))
                    {
                        r += screen[i * fact + k][j * fact + l].r;
                        g += screen[i * fact + k][j * fact + l].g;
                        b += screen[i * fact + k][j * fact + l].b;
                        count++;
                    }
                }
            }

            newscreen[i][j] = get_color(r / count, g / count, b / count);
        }
    }

    return newscreen;
}


static double my_abs(double a)
{
    return (a < 0) ? -a : a;
}

void depth(struct color **screen, double **dist, int w, int h)
{
    struct color **dup = malloc(sizeof (struct color *) * w);

    for (int i = 0; i < w; i++)
    {
        dup[i] = malloc(sizeof (struct color) * h);
        for (int j = 0; j < h; j++)
        {
            dup[i][j] = screen[i][j];
        }
    }


    for (int i = 0; i < w; i++)
    {
        for (int j = 0; j < h; j++)
        {
            if (dist[i][j] > FOCAL / 3  && dist[i][j] < 2 * FOCAL / 3)
                continue;

            int r = 0;
            int g = 0;
            int b = 0;
            int c = 0;
            int lim = my_abs(dist[i][j] - FOCAL) / 5;
            for (int k = - lim; k <= lim; k++)
            {
                for (int l = - lim; l <= lim; l++)
                {
                    if (check(w, h, i + k, j + l))
                    {
                        r += dup[i + k][j + l].r;
                        g += dup[i + k][j + l].g;
                        b += dup[i + k][j + l].b;
                        c++;
                    }
                }
            }
            struct color color = { .r = r / c, .g = g / c, .b = b /c};
            screen[i][j] = color;
        }
    }




    for (int i = 0; i < w; i++)
        free(dup[i]);
    free(dup);
}
