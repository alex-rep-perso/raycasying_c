#include "vector.h"

#ifndef VECTOR2_H
# define VECTOR2_H

double vec_dist(s_vector3f a, s_vector3f b);
s_vector3f vec_normalize(s_vector3f a);
double vec_norm(s_vector3f a);
double vec_norm2(s_vector3f a);
s_vector3f vec_reflexion(s_vector3f a, s_vector3f n);

#endif /* !VECTOR2_H */
