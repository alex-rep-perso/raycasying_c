#include "vector.h"

#ifndef CAMERA_H
# define CAMERA_H
# define PI 3.14159265359
# define FOV 45 * PI / 180

struct camera
{
  s_vector3f pos;
  s_vector3f u;
  s_vector3f v;
  s_vector3f w;
  // distance from camera to the screen
  double l;
  // center of the screen
  s_vector3f c;
  // width and height of the screen
  int width;
  int height;
};

struct ray
{
  s_vector3f pt;
  s_vector3f dir;
};

struct camera init_camera(int width, int height, 
                          s_vector3f pos, s_vector3f u, s_vector3f v);
struct ray generate_ray(int i, int j, struct camera *cam);

#endif 
