# define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "triangle.h"
#include "elements.h"
#include "functions.h"
#include "vector.h"
#include "vector2.h"

void set_n_d(struct triangle *triangle)
{
    s_vector3f ab = vec_sub(triangle->a, triangle->b);
    s_vector3f ac = vec_sub(triangle->a, triangle->c);

    triangle->n = vec_normalize(vec_mul(ab, ac));

    triangle->d = -triangle->n.x * triangle->a.x - triangle->n.y
                * triangle->a.y - triangle->n.z * triangle->a.z;
}


struct triangle *create_triangle(char *parameter)
{
    char *saveptr = NULL;
    strtok_r(parameter, " ", &saveptr);

    struct triangle *triangle = malloc(sizeof (struct triangle));

    triangle->a.x = my_atof(strtok_r(NULL, " ", &saveptr));
    triangle->a.y = my_atof(strtok_r(NULL, " ", &saveptr));
    triangle->a.z = my_atof(strtok_r(NULL, " ", &saveptr));

    triangle->b.x = my_atof(strtok_r(NULL, " ", &saveptr));
    triangle->b.y = my_atof(strtok_r(NULL, " ", &saveptr));
    triangle->b.z = my_atof(strtok_r(NULL, " ", &saveptr));

    triangle->c.x = my_atof(strtok_r(NULL, " ", &saveptr));
    triangle->c.y = my_atof(strtok_r(NULL, " ", &saveptr));
    triangle->c.z = my_atof(strtok_r(NULL, " ", &saveptr));

    set_n_d(triangle);

    triangle->info = get_info(strtok_r(NULL, "\n", &saveptr));

    return triangle;
}


struct list_triangle *add_triangle(struct list_triangle *l, struct triangle *p)
{
    struct list_triangle *new = malloc(sizeof (struct list_triangle));

    new->elt = p;
    new->next = l;

    return new;
}


void free_list_triangle(struct list_triangle *l)
{
    while (l)
    {
        struct list_triangle *tmp = l->next;
        free(l->elt->info);
        free(l->elt);
        free(l);
        l = tmp;
    }
}
