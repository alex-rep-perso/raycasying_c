#include "struct.h"
#include "vector.h"
#include "vector2.h"

#ifndef ELEMENTS_H
# define ELEMENTS_H


struct info
{
    double diff;
    double refl;
    double spec;
    double shin;
    struct color c;
    double refr;
    double opac;
};


struct plane
{
    s_vector3f n;
    double d;
    struct info *info;
};

struct triangle
{
    s_vector3f a;
    s_vector3f b;
    s_vector3f c;
    s_vector3f n;
    double d;
    struct info *info;
};


struct sphere
{
    s_vector3f center;
    double dist;
    double radius;
    struct info *info;
};


struct info *get_info(char *parameters);
struct sphere *create_sphere(s_vector3f pos, char *parameters);
void free_sphere(struct sphere *s);



#endif /* !ELEMENTS_H */
