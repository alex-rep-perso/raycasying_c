#include <math.h>
#include <stdio.h>

#include "vector.h"
#include "vector2.h"
#include "camera.h"

struct camera init_camera(int width, int height,
                          s_vector3f pos, s_vector3f u, s_vector3f v)
{
  struct camera cam;

  cam.pos = pos;
  cam.width = width;
  cam.height = height;
  cam.u = vec_normalize(u);
  cam.v = vec_normalize(v);
  cam.w = vec_mul(u, v);

  cam.l = width / (2 * tan(FOV / 2));
  cam.c = vec_add(pos, vec_scal(cam.l, cam.w));

  printf("vecteur w: %lf  %lf  %lf\n", cam.w.x, cam.w.y, cam.w.z);
  printf("cam pos: %lf  %lf  %lf\n", cam.pos.x, cam.pos.y, cam.pos.z);
  printf("l: %lf\n", cam.l);

  return cam;
}


// i between -width/2 ans width/2, j between -height/2 ans height/2
struct ray generate_ray(int i, int j, struct camera *cam)
{
  struct ray r;
  s_vector3f iu = vec_scal(i, cam->u);
  s_vector3f jv = vec_scal(j, cam->v);
  s_vector3f pt_screen = vec_add(cam->c, vec_add(iu, jv));
  r.pt = cam->pos;

  r.dir = vec_normalize(vec_sub(cam->pos, pt_screen));

  return r;
}
