# define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "camera.h"
#include "elements.h"
#include "vector.h"
#include "vector2.h"
#include "list_sphere.h"
#include "environment.h"
#include "light.h"
#include "render.h"
#include "plane.h"
#include "triangle.h"

#include "processing.h"


void write_ppm(char *filename, struct color **screen, int w, int h)
{
  FILE *f = fopen(filename, "w");
  if (!f)
    exit(1);

  fprintf(f, "P3\n%d %d\n255\n", w, h);
  for (int i = 0; i < h ; i++)
  {
    for (int j = 0; j < w; j++)
      fprintf(f, "%d %d %d ", screen[j][i].r, screen[j][i].g, screen[j][i].b);
    fprintf(f, "\n");
  }

  fclose(f);
}


void set_env(struct env *e, char *line, char *dup)
{
    if (!strncmp(line, "screen", 6))
        set_screen(dup, &e->w, &e->h);
    else if (!strncmp(line, "camera", 6))
        set_camera(e, dup);
    else if (!strncmp(line, "sphere", 6))
    {
        struct sphere *s = create_sphere(e->cam.pos, dup);
        e->l_sphere = add_at(e->l_sphere, s);
    }
    else if (!strncmp(line, "plane", 5))
        e->l_plane = add_plane(e->l_plane, create_plane(line));
    else if (!strncmp(line, "triangle", 8))
        e->l_triangle = add_triangle(e->l_triangle, create_triangle(line));
    else if (!strncmp(line, "alight", 6))
        set_alight(line, e);
    else if (!strncmp(line + 1, "light", 5))
        e->l_light = add_light(e->l_light, get_light(line));
    else if (!strncmp(line, "ssaa", 4))
        e->fact = line[5] - '0';
}



struct env *read_file(char *filename)
{
    FILE *f = fopen(filename, "r");
    if (!f)
        exit(1);

    struct env *e = create_env();
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    while ((read = getline(&line, &len, f)) != -1)
    {
        char *dup = strdup(line);
        set_env(e, line, dup);

        free(dup);
    }

    free(line);
    fclose(f);
    return e;
}


static void free_screen(struct color **screen, int w)
{
    for (int i = 0; i < w; i++)
        free(screen[i]);
    free(screen);
}


int main(int argc, char **argv)
{
    if (argc < 3)
        return 1;


    struct env *e = read_file(argv[1]);
    print_list_sphere(e->l_sphere);

    struct color **big_screen = stretch(&e->w, &e->h, e->fact);
    e->cam.l *= e->fact;
    e->cam.c = vec_add(e->cam.pos, vec_scal(e->cam.l, e->cam.w));

    e->dist = malloc(sizeof (double *) * e->w);
    for (int i = 0 ; i < e->w; i++)
    {
        e->dist[i] = malloc(sizeof (double) * e->h);
        for (int j = 0 ; j < e->h; j++)
            e->dist[i][j] = -1;
    }

    render_screen(e, big_screen);

    depth(big_screen, e->dist, e->w, e->h);

    struct color **small_screen = shrink(big_screen, e->w, e->h, e->fact);

    write_ppm(argv[2], small_screen, e->w / e->fact, e->h / e->fact);
    //write_ppm(argv[2], big_screen, e->w, e->h);

    free_screen(big_screen, e->w);
    free_screen(small_screen, e->w / e->fact);

    free_env(e);

    return 0;
}
