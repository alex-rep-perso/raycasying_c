#include <float.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "raycast.h"
#include "vector.h"
#include "vector2.h"
#include "environment.h"
#include "elements.h"

# define MIN_INTER 0.01f


double find_intersect_sphere(s_vector3f *pt_i, struct ray r,
        struct sphere *s)
{
    double a = vec_norm2(r.dir);
    double b = 2 * vec_dot(r.dir, vec_sub(s->center, r.pt));
    double c = -(s->radius * s->radius) + vec_norm2(vec_sub(s->center, r.pt));

    double delta = b * b - 4 * a * c;
    if (delta < 0)
        return -1;

    double res = (-b - sqrt(delta)) / (2 * a);

    if (res < MIN_INTER)
        res = (-b + sqrt(delta)) / (2 * a);

    *pt_i = vec_add(r.pt, vec_scal(res, r.dir));

    return res;
}

double find_intersect_plan(s_vector3f *pt_i, struct ray r, struct plane *p)
{
    double denom = vec_dot(p->n, r.dir);
    if (denom == 0)
        return -1;


    double t0 = - (vec_dot(p->n, r.pt) + p->d) / denom;
    *pt_i = vec_add(r.pt, vec_scal(t0, r.dir));

    return t0;
}


double find_intersect_tri(s_vector3f *pt_i, struct ray ray, struct triangle *t)
{
    double denom = vec_dot(t->n, ray.dir);
    if (denom == 0)
        return -1;

    double t0 = - (vec_dot(t->n, ray.pt) + t->d) / denom;
    *pt_i = vec_add(ray.pt, vec_scal(t0, ray.dir));

    if (t0 < 0)
        return -1;

    // compute if intersection point is inside the triangle
    s_vector3f ap = vec_sub(t->a, *pt_i);
    s_vector3f bp = vec_sub(t->b, *pt_i);
    s_vector3f cp = vec_sub(t->c, *pt_i);
    s_vector3f ab = vec_sub(t->a, t->b);
    s_vector3f bc = vec_sub(t->b, t->c);
    s_vector3f ca = vec_sub(t->c, t->a);

    s_vector3f apb = vec_mul(ap, ab);
    s_vector3f bpc = vec_mul(bp, bc);
    s_vector3f cpa = vec_mul(cp, ca);

    double c1 = vec_dot(apb, bpc);
    double c2 = vec_dot(apb, cpa);
    double c3 = vec_dot(bpc, cpa);

    if ((c1 < 0 && c2 < 0 && c3 < 0) || (c1 > 0 && c2 > 0 && c3 > 0))
        return t0;

    return -1;
}

double find_intersect(struct env *e, struct ray r, s_vector3f *pt)
{
    struct list_sphere *cur_s = e->l_sphere;
    for (;cur_s; cur_s = cur_s->next)
    {
        if (find_intersect_sphere(pt, r, cur_s->elt) > MIN_INTER)
            return 2 - cur_s->elt->info->opac;
    }

    struct list_plan *cur_p = e->l_plane;
    for (;cur_p; cur_p = cur_p->next)
    {
        if (find_intersect_plan(pt, r, cur_p->elt) > MIN_INTER)
            return (2 - cur_p->elt->info->opac);
    }

    struct list_triangle *cur_t = e->l_triangle;
    for (;cur_t ; cur_t = cur_t->next)
    {
        if (find_intersect_tri(pt, r, cur_t->elt) > MIN_INTER)
            return 2 - cur_t->elt->info->opac;
    }

    return 0;
}

struct plane *find_nearest_plan(double *dist, s_vector3f *pt_i,
        struct ray r, struct list_plan *l_p)
{
    double min_dist = FLT_MAX;
    struct list_plan *cur = l_p;
    struct plane *nearest = NULL;
    s_vector3f pt;

    while (cur != NULL)
    {
        double d = find_intersect_plan(&pt, r, cur->elt);
        if (d > MIN_INTER && d < min_dist)
        {
            nearest = cur->elt;
            min_dist = d;
            *pt_i = pt;
        }
        cur = cur->next;
    }

    *dist = min_dist;
    return nearest;
}

struct triangle *find_nearest_tri(double *dist, s_vector3f *pt_i,
        struct ray r, struct list_triangle *l_t)
{
    double min_dist = FLT_MAX;
    struct list_triangle *cur = l_t;
    struct triangle *nearest = NULL;
    s_vector3f pt;

    while (cur != NULL)
    {
        double d = find_intersect_tri(&pt, r, cur->elt);
        if (d > MIN_INTER && d < min_dist)
        {
            nearest = cur->elt;
            min_dist = d;
            *pt_i = pt;
        }
        cur = cur->next;
    }

    *dist = min_dist;
    return nearest;
}

struct sphere *find_nearest_sphere(double *dist, s_vector3f *pt_i,
        struct ray r, struct list_sphere *l_s)
{
    double min_dist = FLT_MAX;
    struct list_sphere *cur = l_s;
    struct sphere *nearest = NULL;
    s_vector3f pt;

    while (cur != NULL)
    {
        double d = find_intersect_sphere(&pt, r, cur->elt);
        if (d > MIN_INTER && d < min_dist)
        {
            nearest = cur->elt;
            min_dist = d;
            *pt_i = pt;
        }
        cur = cur->next;
    }

    *dist = min_dist;
    return nearest;
}
