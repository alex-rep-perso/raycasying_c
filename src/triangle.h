#ifndef TRIANGLE_H
# define TRIANGLE_H

#include "elements.h"

struct list_triangle
{
    struct list_triangle *next;
    struct triangle *elt;
};



struct triangle *create_triangle(char *parameter);
struct list_triangle *add_triangle(struct list_triangle *l, struct triangle *p);
void free_list_triangle(struct list_triangle *l);

#endif /* !TRIANGLE_H */
