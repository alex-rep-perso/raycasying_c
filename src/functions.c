#include <stdlib.h>
#include <stdio.h>

double my_atof(char *s)
{
    return atof(s);
}

double my_atoi(char *s)
{
    return atoi(s);
}
