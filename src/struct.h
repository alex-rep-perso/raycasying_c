#ifndef STRUCT_H
# define STRUCT_H

struct color
{
    unsigned char r;
    unsigned char g;
    unsigned char b;
};

struct color add_spec(struct color a, double spec);
struct color add_color(struct color a, struct color b);
// 0 < a < 1 !!
struct color scal_color(struct color c, double a);

#endif /* !STRUCT_H */
