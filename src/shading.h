#include "environment.h"
#include "vector.h"
#include "light.h"

#ifndef SHADING_H
# define SHADING_H
# define LA 0.2f

struct color compose_colors(struct color light, struct color obj, double fact);
// the n vector must be normalized !
double calc_diffuse(s_vector3f *point, s_vector3f *n, struct light *l);
double calc_spec(s_vector3f *v, s_vector3f *point, 
                 s_vector3f *n, struct light *l);

#endif /* !SHADING_H */
