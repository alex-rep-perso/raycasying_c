# define _GNU_SOURCE

#include <string.h>
#include <stdlib.h>

#include "elements.h"
#include "vector2.h"
#include "light.h"
#include "functions.h"


struct list_light *add_light(struct list_light *l, struct light *light)
{
    struct list_light *new = malloc(sizeof (struct list_light));

    new->elt = light;
    new->next = l;

    return new;
}


void free_lights(struct list_light *l)
{
    if (l != NULL)
        while (l)
        {
            struct list_light *tmp = l->next;
            free(l->elt);
            free(l);
            l = tmp;
        }
}


struct light *get_light(char *param)
{
    struct light *light = malloc(sizeof (struct light));

    char *saveptr = NULL;

    char *tmp = strtok_r(param, " ", &saveptr);
    if (!strncmp(tmp, "plight", 6))
        light->type = PLIGHT;
    else
        light->type = DLIGHT;

    light->v.x = my_atof(strtok_r(NULL, " ", &saveptr));
    light->v.y = my_atof(strtok_r(NULL, " ", &saveptr));
    light->v.z = my_atof(strtok_r(NULL, " ", &saveptr));

    light->c.r = my_atof(strtok_r(NULL, " ", &saveptr));
    light->c.g = my_atof(strtok_r(NULL, " ", &saveptr));
    light->c.b = my_atof(strtok_r(NULL, " ", &saveptr));

    return light;
}
