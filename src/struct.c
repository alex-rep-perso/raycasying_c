#include "struct.h"


struct color add_spec(struct color a, double spec)
{
    if (a.r + 255 * spec > 255)
        a.r = 255;
    else
        a.r += 255 * spec;

    if (a.g + 255 * spec > 255)
        a.g = 255;
    else
        a.g += 255 * spec;

    if (a.b + 255 * spec > 255)
        a.b = 255;
    else
        a.b += 255 * spec;

    return a;
}

struct color add_color(struct color a, struct color b)
{
    struct color res =
    {
        .r = (a.r + b.r < 255) ? a.r + b.r : 255,
        .g = (a.g + b.g < 255) ? a.g + b.g : 255,
        .b = (a.b + b.b < 255) ? a.b + b.b : 255
    };
    return res;
}

struct color scal_color(struct color c, double a)
{
  c.r *= a;
  c.g *= a;
  c.b *= a;
  return c;
}
