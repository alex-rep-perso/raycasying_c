#include <stdlib.h>
#include <stdio.h>

#include "environment.h"
#include "list_sphere.h"
#include "elements.h"
#include "vector2.h"



struct list_sphere *add_at(struct list_sphere *l, struct sphere *s)
{
    struct list_sphere *new = malloc(sizeof (struct list_sphere));
    if (!new)
        return l;

    new->elt = s;
    new->dist = s->dist;

    if (!l || l->dist > s->dist)
    {
        new->next = l;
        return new;
    }

    struct list_sphere *tmp = l;

    while (tmp->next && tmp->next->dist < new->dist)
        tmp = tmp->next;

    new->next = tmp->next;
    tmp->next = new;

    return l;
}


struct list_sphere *pop_sphere(struct list_sphere * l)
{
    struct list_sphere *first = l;
    l = l->next;

    return first;
}

void free_list_sphere(struct list_sphere *l)
{
    while (l)
    {
        struct list_sphere *tmp = l->next;
        struct sphere *s = l->elt;
        free(s->info);
        free(s);
        free(l);
        l = tmp;
    }
}


struct sphere *get_elt_sphere(struct list_sphere *l)
{
    struct sphere *s = l->elt;
    return s;
}


void print_list_sphere(struct list_sphere *l)
{
    struct list_sphere *tmp = l;

    while (tmp)
    {
        printf("distance: %f\n", tmp->dist);
        tmp = tmp->next;
    }
}
