#include "camera.h"
#include "list_sphere.h"
#include "struct.h"
#include "plane.h"

#ifndef ENVIRONMENT_H
# define ENVIRONMENT_H

struct env
{
    int w;
    int h;
    int fact;
    double **dist;
    struct camera cam;
    struct list_sphere *l_sphere;
    struct list_light *l_light;
    struct list_plan *l_plane;
    struct list_triangle *l_triangle;
    struct color alight;
};

struct env *create_env(void);
void free_env(struct env *e);
void set_alight(char *parameter, struct env *e);
void set_screen(char *parameter, int *w, int *h);
void set_camera(struct env *e, char *parameter);


#endif /* !ENVIRONMENT_H */
