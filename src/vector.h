#ifndef VECTOR_H
# define VECTOR_H

typedef struct 
{
  float x;
  float y;
  float z;
} s_vector3f;

s_vector3f vec_sub(s_vector3f a, s_vector3f b);
s_vector3f vec_scal(double l, s_vector3f a);
s_vector3f vec_add(s_vector3f a, s_vector3f b);
double vec_dot(s_vector3f a, s_vector3f b);
s_vector3f vec_mul(s_vector3f a, s_vector3f b);

#endif /* !VECTOR_H */
