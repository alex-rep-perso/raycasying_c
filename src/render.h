#include "environment.h"
#include "struct.h"

#ifndef RENDER_H
# define RENDER_H
# define MAX_REFLEXION 3


enum elt_type
{
    SPHERE = 0,
    PLANE = 1,
    TRIANGLE = 2
};



void render_screen(struct env *e, struct color **screen);
struct color *get_px(struct color **screen, int i, int j);

#endif /* !RENDER_H */
