#include <math.h>

#include "vector.h"
#include "vector2.h"

double vec_dist(s_vector3f a, s_vector3f b)
{
  double xdiff = b.x - a.x;
  double ydiff = b.y - a.y;
  double zdiff = b.z - a.z;

  return sqrt(xdiff * xdiff + ydiff * ydiff + zdiff * zdiff);
}

s_vector3f vec_normalize(s_vector3f a)
{
  double inv_norm = 1 / vec_norm(a);
  a.x *= inv_norm;
  a.y *= inv_norm;
  a.z *= inv_norm;
  return a;
}

double vec_norm(s_vector3f a)
{
  return sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
}

double vec_norm2(s_vector3f a)
{
  return a.x * a.x + a.y * a.y + a.z * a.z;
}

s_vector3f vec_reflexion(s_vector3f a, s_vector3f n)
{
  double dot = vec_dot(a, n);
  return vec_sub(a, vec_scal(2 * dot, n));
}
