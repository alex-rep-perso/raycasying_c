#include "vector.h"

s_vector3f vec_sub(s_vector3f a, s_vector3f b)
{
  s_vector3f sub =
  {
    .x = b.x - a.x,
    .y = b.y - a.y,
    .z = b.z - a.z
  };
  return sub;
}

s_vector3f vec_add(s_vector3f a, s_vector3f b)
{
  s_vector3f add =
  {
    .x = b.x + a.x,
    .y = b.y + a.y,
    .z = b.z + a.z
  };
  return add;
}

s_vector3f vec_scal(double l, s_vector3f a)
{
  a.x *= l;
  a.y *= l;
  a.z *= l;
  return a;
}

s_vector3f vec_mul(s_vector3f a, s_vector3f b)
{
  s_vector3f mul =
  {
    .x = a.y * b.z - a.z * b.y,
    .y = a.z * b.x - a.x * b.z,
    .z = a.x * b.y - a.y * b.x
  };

  return mul;
}

double vec_dot(s_vector3f a, s_vector3f b)
{
  return a.x * b.x + a.y * b.y + a.z * b.z;
}
