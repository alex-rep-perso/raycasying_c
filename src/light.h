#ifndef LIGHT_H
# define LIGHT_H

#include "elements.h"
#include "vector2.h"
#include "struct.h"

enum light_type
{
    PLIGHT,
    DLIGHT
};


struct light
{
    enum light_type type;
    s_vector3f v;
    struct color c;
};


struct list_light
{
    struct light *elt;
    struct list_light *next;
};

struct list_light *add_light(struct list_light *l, struct light *light);
struct light *get_light(char *param);
void free_lights(struct list_light *l);



#endif /* !LIGHT_H */
