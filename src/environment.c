# define _GNU_SOURCE

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "vector.h"
#include "camera.h"
#include "list_sphere.h"
#include "environment.h"
#include "functions.h"
#include "light.h"
#include "plane.h"
#include "triangle.h"

struct env *create_env(void)
{
    struct env *e = malloc(sizeof (struct env));
    if (!e)
        return NULL;

    e->fact = 1;
    e->w = 0;
    e->h = 0;
    e->l_sphere = NULL;
    e->l_light = NULL;
    e->l_plane = NULL;
    e->dist = NULL;

    struct color black =
    {
        .r = 0,
        .g = 0,
        .b = 0
    };

    e->alight = black;

    return e;
}

void free_env(struct env *e)
{
    free_list_sphere(e->l_sphere);
    free_list_plan(e->l_plane);
    free_list_triangle(e->l_triangle);
    free_lights(e->l_light);

    for (int i = 0 ; i < e->w; i++)
        free(e->dist[i]);
    free(e->dist);

    free(e);
}

void set_alight(char *parameter, struct env *e)
{
    char *saveptr = NULL;
    char *tmp = strtok_r(parameter, " ", &saveptr);
    tmp = tmp;

    e->alight.r = my_atoi(strtok_r(NULL, " ", &saveptr));
    e->alight.g = my_atoi(strtok_r(NULL, " ", &saveptr));
    e->alight.b = my_atoi(strtok_r(NULL, " ", &saveptr));
}


void set_screen(char *parameter, int *w, int *h)
{
    char *tmp = parameter;
    char *saveptr = NULL;

    tmp = strtok_r(tmp, " ", &saveptr);

    *w = my_atoi(strtok_r(NULL, " ", &saveptr));
    *h = my_atoi(strtok_r(NULL, " ", &saveptr));
}

void set_camera(struct env *e, char *parameter)
{
    char *saveptr = NULL;
    char *tmp = strtok_r(parameter, " ", &saveptr);
    tmp = tmp;

    s_vector3f u;
    u.x = my_atof(strtok_r(NULL, " ", &saveptr));
    u.y = my_atof(strtok_r(NULL, " ", &saveptr));
    u.z = my_atof(strtok_r(NULL, " ", &saveptr));

    s_vector3f v;
    v.x = my_atof(strtok_r(NULL, " ", &saveptr));
    v.y = my_atof(strtok_r(NULL, " ", &saveptr));
    v.z = my_atof(strtok_r(NULL, " ", &saveptr));

    s_vector3f w;
    w.x = my_atof(strtok_r(NULL, " ", &saveptr));
    w.y = my_atof(strtok_r(NULL, " ", &saveptr));
    w.z = my_atof(strtok_r(NULL, " ", &saveptr));

    e->cam = init_camera(e->w, e->h, u, v, w);
}


