#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "vector.h"
#include "render.h"
#include "shading.h"
#include "camera.h"
#include "raycast.h"
#include "environment.h"
#include "list_sphere.h"
#include "struct.h"

static struct color black =
{
  .r = 0,
  .g = 0,
  .b = 0
};

struct color ray_cast(struct ray r, struct env *e, int nb_r, double *dlist_l);

struct color *get_px(struct color **screen, int i, int j)
{
    return &screen[i][j];
}


static struct ray get_ray(s_vector3f pt, struct light *light)
{
    struct ray r;
    r.pt = pt;

    if (light->type == PLIGHT)
        r.dir = vec_normalize(vec_sub(pt, light->v));
    else
        r.dir = vec_normalize(vec_scal(-1, light->v));

    return r;
}


double calc_opac_through(struct env *e, struct ray r)
{
  s_vector3f pt;
  double inter = find_intersect(e, r, &pt);
  double n = 0;
  if (inter == 0)
    return 1;

  double res_coeff = 0;
  while (inter > 1 + 0.01f && inter != 0)
  {
    res_coeff += (inter - 1);
    ++n;
    struct ray r_continue =
    {
      .pt = pt,
      .dir = r.dir
    };
    inter = find_intersect(e, r_continue, &pt);
  }
  if (inter == 1)
    return 0;
  return res_coeff / n;
}

struct color calc_px(s_vector3f *pt, struct info *info, s_vector3f n,
                     struct env *e, s_vector3f view, double *dist_l)
{
  struct color res = compose_colors(e->alight, info->c, LA);
  struct list_light *cur = e->l_light;

  if (vec_dot(view, n) < 0)
    n = vec_scal(-1, n);

  if (dist_l)
  {
    double tmp = vec_dist(*pt, e->cam.pos);
    if (tmp)
        *dist_l = tmp;
  }

  while (cur != NULL)  
  {
    double opac_coeff = calc_opac_through(e, get_ray(*pt, cur->elt));
    if (opac_coeff > 0)
    {
      double diff = info->diff * calc_diffuse(pt, &n, cur->elt);
      struct color d = compose_colors(cur->elt->c, info->c, diff);
      d = scal_color(d, opac_coeff);

      double spec = calc_spec(&view, pt, &n, cur->elt);
      if (spec != 0)
        spec = opac_coeff * info->spec * pow(spec, info->shin);
      res = add_color(res, d);

      res = add_spec(res, spec);
      res = scal_color(res, opac_coeff);
    }
    cur = cur->next;
  }
  return res;
}


void swap(double *d1, double *d2)
{
    double type_elt = d1[1];
    double d = d1[0];

    d1[0] = d2[0];
    d1[1] = d2[1];

    d2[0] = d;
    d2[1] = type_elt;
}


void *find_nearest(double **dist, struct ray r, s_vector3f *pt,
                struct env *e)
{
    s_vector3f pt_s;
    struct sphere *s = find_nearest_sphere(&dist[0][0], &pt_s, r, e->l_sphere);

    s_vector3f pt_p;
    struct plane *pl = find_nearest_plan(&dist[1][0], &pt_p, r, e->l_plane);
 
    s_vector3f pt_t;
    struct triangle *t = find_nearest_tri(&dist[2][0], &pt_t, r, e->l_triangle);

    int d0d1 = dist[0][0] > dist[1][0];
    int d0d2 = dist[0][0] > dist[2][0];
    int d1d2 = dist[1][0] > dist[2][0];

    if (d0d2 * (d1d2 || !d0d1))
    {
        swap(dist[0], dist[2]);
        *pt = pt_t;
        return t;
    }
    else if (d0d1 * (!d1d2 || !d0d2))
    {
        swap(dist[0], dist[1]);
        *pt = pt_p;
        return pl;
    }
    else
    {
        *pt = pt_s;
        return s;
    }
}

struct color calc_refraction(s_vector3f *pt, const s_vector3f view, 
                       s_vector3f normal, struct env *e, int nb_r, double refr)
{
  struct color res = black;
  double dot = vec_dot(view, normal);
  
  if (dot < 0)
  {
    refr = 1 / refr;
    normal = vec_scal(-1, normal);
  }
 
  
  double coef_refract = 1 - refr * refr * (1 - dot * dot);
  if (coef_refract >= 0)
  {
    double coef_n = refr * dot - sqrt(coef_refract);
    normal = vec_scal(coef_n, normal);
    // transmission vector
    s_vector3f t = vec_sub(vec_scal(refr, view), normal);
    struct ray r_refract = 
    {
      .pt = *pt,
      .dir = t
    };
    res = ray_cast(r_refract, e, nb_r + 1, NULL);
  }

  return res;
}

struct color calc_reflexion(const s_vector3f view, s_vector3f normal,
                            struct env *e, int nb_r, s_vector3f *pt)
{
  struct color res = black;
  if (nb_r < MAX_REFLEXION)
  {
    s_vector3f ref_view = vec_reflexion(view, normal);
    struct ray r =
    {
      .pt = *pt,
      .dir = ref_view
    };
    res = ray_cast(r, e, nb_r + 1, NULL);
  }
  return res;
}


struct color shade(s_vector3f pt, struct info *info, const s_vector3f n,
                   struct env *e, const struct ray r, int nb_r, double *dlist_l)
{
  s_vector3f view = vec_scal(-1, r.dir);
  double coeff_rem = info->opac;
  struct color res = black;
  struct color refr = black;

  double coeff_refx = (nb_r < MAX_REFLEXION) ? info->refl : 0;
  res = scal_color(calc_px(&pt, info, n, e, view, dlist_l), 
                   coeff_rem * (1 - coeff_refx));

  if (coeff_rem < 1)
    refr = scal_color(calc_refraction(&pt, view, n, e, nb_r, 
                      info->refr), 1 - coeff_rem);

  struct color refc = scal_color(calc_reflexion(view, n, e, nb_r, &pt),
                                 coeff_rem * coeff_refx);
  res = add_color(res, refr);
  res = add_color(res, refc);
  return res;
}


struct color ray_cast(struct ray r, struct env *e, int nb_r, double *dlist_l)
{
  double **dist = malloc(sizeof (double *) * 3);
  for (int i = 0; i < 3; i++)
  {
    dist[i] = calloc(2, sizeof (double));
    dist[i][1] = i;
  }

  s_vector3f pt;
  void *res_ptr = find_nearest(dist, r, &pt, e);

  struct color res = black;
  
  if (!res_ptr)
    res = black;
  else if (dist[0][1] == SPHERE)
  {
    struct sphere *sph = res_ptr;
    s_vector3f n = vec_normalize(vec_sub(sph->center, pt));
    res = shade(pt, sph->info, n, e, r, nb_r, dlist_l);
 }
  else if (dist[0][1] == PLANE)
  {
    struct plane *pl = res_ptr;
    s_vector3f n = vec_scal(-1, pl->n);
    res = shade(pt, pl->info, n, e, r, nb_r, dlist_l);
  }
  else
  {
    struct triangle *tr = res_ptr;
    res = shade(pt, tr->info, tr->n, e, r, nb_r, dlist_l);
  }

  for (int i = 0; i < 3; i++)
    free(dist[i]);
  free(dist);

  return res;
}

void render_screen(struct env *e, struct color **screen)
{
    int w = e->w / 2;
    int h = e->h / 2;

    for (int i = -w; i < w; ++i)
        for (int j = -h; j < h; ++j)
        {
            struct ray r = generate_ray(i, j, &e->cam);
            struct color *c = get_px(screen, i + w, j + h);
            double dist = 0;
            *c = ray_cast(r, e, 0, &dist);
            e->dist[i + w][j + h] = dist;
        }
}
