# define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "plane.h"
#include "elements.h"
#include "functions.h"


struct plane *create_plane(char *parameter)
{
    char *saveptr = NULL;
    strtok_r(parameter, " ", &saveptr);

    struct plane *plane = malloc(sizeof (struct plane));

    plane->n.x = my_atof(strtok_r(NULL, " ", &saveptr));
    plane->n.y = my_atof(strtok_r(NULL, " ", &saveptr));
    plane->n.z = my_atof(strtok_r(NULL, " ", &saveptr));

    plane->n = vec_normalize(plane->n);

    plane->d = my_atof(strtok_r(NULL, " ", &saveptr));

    printf("plane dist: %lf\n", plane->d);

    plane->info = get_info(strtok_r(NULL, "\n", &saveptr));

    return plane;
}


struct list_plan *add_plane(struct list_plan *l, struct plane *p)
{
    if (!p)
        return NULL;

    struct list_plan *new = malloc(sizeof (struct list_plan));

    new->elt = p;
    new->next = l;

    return new;
}


void free_list_plan(struct list_plan *l)
{
    while (l)
    {
        struct list_plan *tmp = l->next;
        free(l->elt->info);
        free(l->elt);
        free(l);
        l = tmp;
    }
}
