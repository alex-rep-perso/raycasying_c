#ifndef PROCESSING_H
# define PROCESSING_H

struct color **stretch(int *w, int *h, int fact);
struct color **shrink(struct color **screen, int w, int h, int fact);
void depth(struct color **screen, double **dist, int w, int h);

#endif /* !PROCESSING_H */
