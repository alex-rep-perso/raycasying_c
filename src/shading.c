#include <stdio.h>

#include "environment.h"
#include "shading.h"
#include "vector.h"
#include "vector2.h"

struct color compose_colors(struct color light, struct color obj, double fact)
{
  struct color res =
  {
    .r = light.r * obj.r * fact / 255,
    .g = light.g * obj.g * fact / 255,
    .b = light.b * obj.b * fact / 255
  };
  return res;
}

double calc_diffuse(s_vector3f *point, s_vector3f *n, struct light *l)
{
  double res = 0;
  if (l->type == DLIGHT)
  {
    s_vector3f l_vec = vec_normalize(vec_scal(-1, l->v));
    res = vec_dot(*n, l_vec);
  }
  else
  {
    double att = 1 / vec_dist(l->v, *point);
    s_vector3f l_vec = vec_normalize(vec_sub(*point, l->v));
    res =  att * vec_dot(*n, l_vec);
  }

    if (res < 0)
        return 0;

    return res;
}

double calc_spec(s_vector3f *v, s_vector3f *point,
                 s_vector3f *n, struct light *l)
{
  s_vector3f l_vec;
  if (l->type == DLIGHT)
    l_vec = vec_normalize(vec_scal(-1, l->v));
  else
    l_vec = vec_normalize(vec_sub(*point, l->v));

  s_vector3f r_vec = vec_reflexion(l_vec, *n);
  double res = vec_dot(r_vec, *v);
  if (res < 0)
    return 0;
  return res;
}


